
public class TestCreature {

	public static final int CREATURE_COUNT = 6;
	public static final int THING_COUNT = 10;
	
	public static void main(java.lang.String[] args) {
		Thing ThingArray[] = new Thing[THING_COUNT];
		Thing ThingDescription[] = new Thing[THING_COUNT];
		Creature CreatureArray[] = new Creature[CREATURE_COUNT];
		Ant Ant1 = new Ant("Andrew");
		Bat Bat1 = new Bat("Ben");
		Fly Fly1 = new Fly("Frederic");
		Tiger Tiger1 = new Tiger("Tom");
		Ant Ant2 = new Ant("Alex");
		Bat Bat2 = new Bat("Bob");
		CreatureArray[0] = Ant1;
		CreatureArray[1] = Bat1;
		CreatureArray[2] = Fly1;
		CreatureArray[3] = Tiger1;
		CreatureArray[4] = Ant2;
		CreatureArray[5] = Bat2;
		ThingArray[0] = new Thing("Computer");
		ThingArray[1] = new Thing("Mouse");
		ThingArray[2] = new Thing("Keyboard");
		ThingArray[3] = new Thing("Pen");
		ThingDescription[0] = new Thing("What we are studying.\n");
		ThingDescription[1] = new Thing("Used to maneuver on the screen.\n");
		ThingDescription[2] = new Thing("Used to type.\n");
		ThingDescription[3] = new Thing("Used to write.\n");
		
		for (int i=4; i<THING_COUNT; i++){
			ThingArray[i] = CreatureArray[i-4];
		}
		
		System.out.println("Things:\n");
		for (int j=0; j<THING_COUNT-6; j++){
			
			System.out.println(ThingArray[j] + ":");
			System.out.println(ThingDescription[j]);
		}
		
		System.out.println("Creatures:\n");
		for(int k=0; k<THING_COUNT; k++){
			if(ThingArray[k] instanceof Creature){
				System.out.println(ThingArray[k]);
			}
				
		}
		
		System.out.print("\n");
		
		Ant1.eat(Bat1);
		Bat1.eat(Fly1);
		Fly1.eat(Tiger1);
		Tiger1.eat(Ant2);
		Bat2.eat(ThingArray[0]);
		
		Ant1.whatDidYouEat();
		Bat1.whatDidYouEat();
		Fly1.whatDidYouEat();
		Tiger1.whatDidYouEat();
		Ant2.whatDidYouEat();
		
		Ant1.move();
		Bat1.move();
		Fly1.move();
		Tiger1.move();

		
	}
}