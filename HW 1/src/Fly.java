
public class Fly extends Creature implements Flyer {

	public Fly(java.lang.String Flyname){
		super(Flyname);
	}
	
	public void eat(Thing aThing){
		if (aThing instanceof Creature){
			System.out.println(name + " " + getClass().getSimpleName() + " won't eat " + aThing);
		}
		else {
			Stomach = aThing;
			System.out.println(name + " has just eaten " + Stomach);	
		}
	}
	
	public void move(){
		fly();
	}
	
	public void fly(){
		System.out.println(name + " " + getClass().getSimpleName() + " is buzzing around in flight.");
	}
	
		
}	