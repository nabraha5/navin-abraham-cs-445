
public abstract class Creature extends Thing {
	
	public Thing Stomach;
	
	public Creature(String Creaturename){
		super(Creaturename);
	}
	
	void eat(Thing aThing) {
		Stomach = aThing;
		System.out.println(name + " just ate " + Stomach);
	}
	
	abstract void move();
	
	void whatDidYouEat() {
		if (Stomach == null) {
			System.out.println(name + " " + getClass().getSimpleName() + " has had nothing to eat!");
		}
		else 
		System.out.println(name + " " + getClass().getSimpleName() + " has eaten " + Stomach + "!");
		}
		
	}
	
	
	

