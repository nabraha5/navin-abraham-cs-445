
public class Bat extends Creature implements Flyer {

	public Bat(java.lang.String Batname){
		super(Batname);
	}
	
	public void eat(Thing aThing){
		if (aThing instanceof Creature){
			Stomach = aThing;
			System.out.println(name + " has just eaten " + Stomach);
		}
		else{
			System.out.println(name + " " + getClass().getSimpleName()+ " won't eat "+ aThing);
		}
	}
	
	public void move(){
		fly();
	}
	
	public void fly(){
		System.out.println(name + " " + getClass().getSimpleName() + " is swooping through the dark.");
	}
	
		
}
