import static org.junit.Assert.*;

import org.junit.Test;


public class UnitTest {

	@Test
	public void test() {
			Thing Thing1 = new Thing("Computer");
			Ant Ant1 = new Ant("Andrew");
			Bat Bat1 = new Bat("Ben");
			Fly Fly1 = new Fly("Frederic");
			Tiger Tiger1 = new Tiger("Tom");
			
			//Test for toString
			assertEquals("Computer", Thing1.toString());
						
			//Tests the move function for all creatures
			System.out.println("Returns 'Andrew Ant is crawling around.'");
			Ant1.move();
			System.out.println("\nReturns 'Ben Bat is swooping through the dark.'");
			Bat1.move();
			System.out.println("\nReturns 'Frederic Fly is buzzing around in flight.'");
			Fly1.move();
			System.out.println("\nReturns 'Tom Tiger has just pounced.'");
			Tiger1.move();		
						
			//Tests the eat function for all creatures
			System.out.println("\nReturns 'Andrew just ate Ben'");
			Ant1.eat(Bat1);
			System.out.println("\nReturns 'Ben Bat won't eat Computer'");
			Bat1.eat(Thing1);
			System.out.println("\nReturns 'Ben has just eaten Frederic'");
			Bat1.eat(Fly1);
			System.out.println("\nReturns 'Frederic Fly won't eat Tom'");
			Fly1.eat(Tiger1);
			System.out.println("\nReturns 'Frederic has just eaten Computer'");
			Fly1.eat(Thing1);
			System.out.println("\nReturns 'Tom just ate Andrew'");
			Tiger1.eat(Ant1);
						
						
			//Test for whatDidYouEat function for all creatures
			System.out.println("\nReturns 'Andrew Ant has eaten Ben!'");
			Ant1.whatDidYouEat();
			System.out.println("\nReturns 'Ben Bat has eaten Frederic!'");
			Bat1.whatDidYouEat();
			System.out.println("\nReturns 'Frederic Fly has eaten Computer!'");
			Fly1.whatDidYouEat();
			System.out.println("\nReturns 'Tom Tiger has eaten Andrew!'");
			Tiger1.whatDidYouEat();
				

		}
			
			

	}


