import static org.junit.Assert.*;

import org.junit.Test;


public class TokenTest {

	@Test
	public void test() {
		
		String strVal[];
		ImprovedStringTokenizer object=new ImprovedStringTokenizer("This is a test");
	
		strVal=object.getStringArray();
	
		assertEquals("strVal[0] should be 'This'", "This", strVal[0]);
		assertEquals("strVal[1] should be 'is'", "is", strVal[1]);
		assertEquals("strVal[2] should be 'a'", "a", strVal[2]);
		assertEquals("strVal[0] should be 'test'", "test", strVal[3]);
		
	}

}
