import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;


public class ImprovedRandomTest {

	@Test
	public void test() {
		ImprovedRandom myclass=new ImprovedRandom();
		double lower = Math.random()*10;
		double higher = Math.random()*10;
		while(true)
		{
			if (higher > lower) 
			{
				break;
			}
			else 
			{
				higher = Math.random()*10;
			}
		}
		
		int ilower = (int) lower;
		int ihigher = (int) higher;
		assertTrue("The answer should be between the two bounds", myclass.getIntBetween(ilower, ihigher) > ilower && myclass.getIntBetween(ilower, ihigher) < ihigher);
	}

}
