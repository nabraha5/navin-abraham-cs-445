import java.io.ObjectInputStream.GetField;
import java.util.Random;

public class ImprovedRandom extends Random {
	public double getdouble()
	{
		return nextDouble();
	}

	public double getinteger()
	{
		return nextInt();
	}

	public int getIntBetween(int small,int large)
	{
		
		int k;
		while(true)
		{
			k=nextInt();
			if(k>small &&k<large)
				break;
		}
		return k;
		
	}
}
