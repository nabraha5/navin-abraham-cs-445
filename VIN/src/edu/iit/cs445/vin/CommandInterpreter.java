package edu.iit.cs445.vin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.time.Year;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.json.JSONObject;

public class CommandInterpreter implements Serializable {
	
	private Club club;
	
	public CommandInterpreter(Club club){
		this.club = club;
	}
	
	private static JSONObject readJSONObject(String filePath) {
try{
			
			String content = null;
			BufferedReader br = new BufferedReader(new FileReader(filePath));
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		            sb.append(line);
		            sb.append("\n");
		            line = br.readLine();
		        }
		       content =  sb.toString();
		    } finally {
		        br.close();
		    }
			
			
		   return  new JSONObject(content);
		    
		    
		}catch(Exception e){
			System.out.println(e);
		}

		return null;

	}
	
	public void processArguments(List<String> args) {
	try
	{
		if(args.get(0).equalsIgnoreCase("subscriber"))
		{
			processSubscriber(args.subList(1, args.size()));
		}
		
		if(args.get(0).equalsIgnoreCase("admin"))
		{
			processAdmin(args.subList(1, args.size()));
		}
		
		if(args.get(0).equalsIgnoreCase("partner"))
		{
			processPartner(args.subList(1, args.size()));
		}
		
		if(args.get(0).equalsIgnoreCase("other"))
		{
			processOther(args.subList(1, args.size()));
		}
	}catch(Exception e){
System.out.println("Arguments: <subscriber/admin/partner/other> <command> ");
	}
	}
	
	
	private void processSubscriber(List<String> args) {
	try
	{
		if(args.get(0).equalsIgnoreCase("add"))
		{
			addSubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("load"))
		{
			loadSubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("modify"))
		{
			modifySubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		
		else if(args.get(0).equalsIgnoreCase("view"))
		{
			viewSubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("search"))
		{
			searchSubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("shipments"))
		{
			shipmentsSubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("shipmentsearch"))
		{
			shipmentSearchSubscriber(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("shipmentnotes"))
		{
			shipmentNotes(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("createnote"))
		{
			createNote(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("notesearch"))
		{
			searchNotes(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("updatenote"))
		{
			updateNote(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("deletenote"))
		{
			deleteNote(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("wines"))
		{
			getWines(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("winesearch"))
		{
			searchWines(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("winenotes"))
		{
			getWineNotes(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("createwinenote"))
		{
			createWineNote(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("winenotesearch"))
		{
			searchWineNotes(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("updatewinenote"))
		{
			updateWineNote(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("deletewinenote"))
		{
			deleteWineNote(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("rating"))
		{
			getWineRating(args.subList(1, args.size()).toArray(new String[]{}));
		}
		
		else if(args.get(0).equalsIgnoreCase("addrating"))
		{
			submitWineRating(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("delivery"))
		{
			getDelivery(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("deliveryupdate"))
		{
			deliveryUpdate(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("changedelivery"))
		{
			deliveryChange(args.subList(1, args.size()).toArray(new String[]{}));
		}
		else if(args.get(0).equalsIgnoreCase("changepreference"))
		{
			preferenceChange(args.subList(1, args.size()).toArray(new String[]{}));
		}
	}
	catch (Exception e)
	{
		System.out.println("Commands: \nadd\nload\nmodify\nview\n"
				+ "search\nshipments\nshipmentsearch\nshipmentnotes\ncreatenote\n"
				+ "notesearch\nupdatenote\ndeletenote\nwines\nwinesearch\n"
				+ "winenotes\ncreatewinenote\nwinenotesearch\nupdatewinenote\n"
				+ "deletewinenote\nrating\naddrating\ndelivery\ndeliveryupdate\nchangedelivery\nchangepreference");	
	}
	}
	
	private void processAdmin(List<String> args) {
	try{
		if(args.get(0).equalsIgnoreCase("add"))
		{
			addAdmin(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("admins"))
		{
			getAdmins(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("adminupdate"))
		{
			updateAdmin(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("adminsearch"))
		{
			adminSearch(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("revenue"))
		{
			getRevenue(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("monthlyselections"))
		{
			getMonthlySelections(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("createmonthlyselection"))
		{
			createMonthlySelection(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("monthlyselectionsearch"))
		{
			searchMonthlySelection(args.subList(1, args.size()).toArray(new String[]{}));
		}
	}
		catch (Exception e)
		{
			System.out.println("Commands: \nadd\nadmins\n"
					+ "adminupdate\nadminsearch\nrevenue\nmonthlyselections\n"
					+ "createmonthlyselection\nmonthlyselectionsearch");	
		}
	}
	
	private void processPartner(List<String> args) {
	try{
		if(args.get(0).equalsIgnoreCase("deliveries"))
		{
			pendingDeliveries(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("createreceipt"))
		{
			createReceipt(args.subList(1, args.size()).toArray(new String[]{}));
		}
		if(args.get(0).equalsIgnoreCase("receipts"))
		{
			System.out.println(this.club.getReceipts());
		}
		if(args.get(0).equalsIgnoreCase("receiptsearch"))
		{
			searchReceipts(args.subList(1, args.size()).toArray(new String[]{}));
		}
	}
	catch (Exception e)
	{
		System.out.println("Commands: \ndeliveries\ncreatereceipt\n"
				+ "receipts\nreceiptsearch");	
	}
	}
	
	private void processOther(List<String> args) {
	try{
		if(args.get(0).equalsIgnoreCase("winesearch"))
		{
			searchAllWines(args.subList(1, args.size()).toArray(new String[]{}));
		}
	}
	catch (Exception e)
	{
		System.out.println("Commands: \nwinesearch");	
	}
	}
	
	private void searchAllWines(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
	
		Option wid = new Option("wid", "wid", true, "WID of Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int wVal = Integer.parseInt(line.getOptionValue("wid"));
		    try{
		    	System.out.println(Wine.widSearch(this.club, wVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main other winesearch", options );
			    
		    
		}
		    
	}
	
	private void searchReceipts(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
	
		Option rid = new Option("rid", "rid", true, "RID of Receipt" );
		rid.setRequired(true);
		options.addOption(rid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int ridVal = Integer.parseInt(line.getOptionValue("rid"));
		    try{
		    	System.out.println(Receipt.searchReceipts(this.club, ridVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main partner receiptsearch", options );
			    
		    
		}
		    
	}
	
	private void createReceipt(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option name = new Option("n", "name", true, "Ordering Subscriber's name" );
		name.setRequired(true);
		options.addOption(name);
		
		Option rb = new Option("rb", "received by", true, "Person accepting delivery" );
		rb.setRequired(true);
		options.addOption(rb);
		
		Option uid = new Option("uid", "uid", true, "UID of ordering Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option sid = new Option("sid", "sid", true, "SID of the shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    String nVal = line.getOptionValue("name");
		    String rbVal = line.getOptionValue("rb");
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    try{
		    	Receipt r = new Receipt(uidVal, sidVal, nVal, rbVal);
		    	this.club.addReceipt(r);
		    	System.out.println(r);
		    	Shipment.sidSearch(this.club, uidVal, sidVal).setStatus("Delivered");
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main partner createreceipt", options );
			    
		    
		}
		    
	}
	
	private void pendingDeliveries(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    try{
		    	System.out.println(DeliveryPartner.getDeliveries(this.club));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main partner deliveries", options );
			    
		    
		}
		    
	}
	
	private void searchMonthlySelection(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option mid = new Option("mid", "mid", true, "MID Of the Monthly Selection" );
		mid.setRequired(true);
		options.addOption(mid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int midVal = Integer.parseInt(line.getOptionValue("mid"));
		    try{
		    	System.out.println(Admin.searchMonthlySelections(this.club, midVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin monthlyselectionsearch ", options );
			    
		    
		}
		    
	}
	
	private void createMonthlySelection(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option mst = new Option("mst", "Monthly Selection Type", true, "Type Of the Monthly Selection" );
		mst.setRequired(true);
		options.addOption(mst);
		
		Option ym = new Option("ym", "Year and Month", true, "Year and Month Of the Monthly Selection" );
		ym.setRequired(true);
		options.addOption(ym);
		
		Option jfile = new Option("f", "jfile", true, "Path for JSON File containing Wine Selection" );
		jfile.setRequired(true);
		options.addOption(jfile);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    String mstVal = line.getOptionValue("mst");
		    String ymVal = line.getOptionValue("ym");
		    String fVal = line.getOptionValue("jfile");
		    JSONObject object = readJSONObject(fVal);
			if(object == null){
				System.out.println("Invalid Json File");
			}
			
			Collection<Wine> wset = new HashSet<Wine>();
			for (int i=0; i< object.getJSONArray("wines").length(); i++) {
		
			String vVal = object.getJSONArray("wines").getJSONObject(i).getString("variety");
			String wtVal = object.getJSONArray("wines").getJSONObject(i).getString("wine_type");
			String lnVal = object.getJSONArray("wines").getJSONObject(i).getString("label_name");
			String gVal = object.getJSONArray("wines").getJSONObject(i).getString("grape");
			String rVal = object.getJSONArray("wines").getJSONObject(i).getString("region");
			String cVal = object.getJSONArray("wines").getJSONObject(i).getString("country");
			String mVal = object.getJSONArray("wines").getJSONObject(i).getString("maker");
			String yVal = object.getJSONArray("wines").getJSONObject(i).getString("year");
			Wine w = new Wine(vVal, wtVal, lnVal, gVal, rVal, cVal, mVal, Year.parse(yVal));
			wset.add(w);
			this.club.addWine(w);
			}
		    try{
		    	MonthlySelection ms = new MonthlySelection(mstVal, ymVal, wset);
		    	this.club.addMS(ms);
		    	GenerateShipments.generateShipments(this.club, mstVal, ymVal, ms);
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin createmonthlyselection", options );
			    
		    
		}
		    
	}
	
	private void getMonthlySelections(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		   
		    try{
		    		System.out.println("Monthly Selections: " + this.club.getMonthlySelections());
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin monthlyselections", options );
			    
		    
		}
		    
	}
	
	private void getRevenue(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option start = new Option("s", "start", true, "Start Date" );
		options.addOption(start);
		
		Option end = new Option("e", "end", true, "End Date" );
		options.addOption(end);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    
		    String sVal = "";
		    String eVal = "";
		    
		    if(line.hasOption("start"))
		    {
		    sVal = line.getOptionValue("start");
		    }
		    
		    if(line.hasOption("end"))
		    {
		    eVal = line.getOptionValue("end");
		    }
		    try{
		    	if(line.hasOption("start") && line.hasOption("end"))
			    {
		    		Admin.adminRevenueStartEnd(this.club, sVal, eVal);
			    }
		    	else
		    	{
		    		Admin.adminRevenueAll(this.club);
		    	}
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin revenue", options );
			    
		    
		}
		    
	}
	
	private void adminSearch(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option aid = new Option("aid", "aid", true, "AID Of the Admin" );
		aid.setRequired(true);
		options.addOption(aid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int aidVal = Integer.parseInt(line.getOptionValue("aid"));
		    try{
		    	System.out.println(Admin.adminSearch(this.club, aidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin adminsearch", options );
			    
		    
		}
		    
	}
	
	private void updateAdmin(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option name = new Option("n", "name", true, "Name Of the Admin" );
		name.setRequired(true);
		options.addOption(name);
		
		Option aid = new Option("aid", "aid", true, "AID Of the Admin" );
		aid.setRequired(true);
		options.addOption(aid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    String nVal = line.getOptionValue("name");
		    int aidVal = Integer.parseInt(line.getOptionValue("aid"));
		    try{
		    	Admin.adminUpdate(this.club, aidVal, nVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin adminupdate", options );
			    
		    
		}
		    
	}
	
	private void getAdmins(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    try{
		    	System.out.println("Admins: " + this.club.getAdmins());
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin admins", options );
			    
		    
		}
		    
	}
	
	private void addAdmin(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option name = new Option("n", "name", true, "Name Of the Admin" );
		name.setRequired(true);
		options.addOption(name);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    String nVal = line.getOptionValue("name");
		    try{
		    	Admin a = new Admin(nVal);
		    	this.club.addAdmin(a);
		    	System.out.println(a);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main admin add", options );
			    
		    
		}
		    
	}
	
	private void preferenceChange(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option preference = new Option("p", "preference", true, "Preference of the Subscriber" );
		preference.setRequired(true);
		options.addOption(preference);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    String pVal = line.getOptionValue("preference");
		    try{
		    		Subscriber.getSubscriber(this.club, uidVal).setPreference(pVal);
		    		System.out.println(Subscriber.getSubscriber(this.club, uidVal));
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber changepreference", options );
			    
		    
		}
		    
	}
	
	private void deliveryChange(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		Option noc = new Option("noc", "noc", true, "Number of Cases" );
		noc.setRequired(true);
		options.addOption(noc);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    int nocVal = Integer.parseInt(line.getOptionValue("noc"));
		    try{
		    	Shipment.sidSearch(club, uidVal, sidVal).updateNumberOfCases(nocVal);
		    	if (nocVal == 0)
		    	{
		    		Shipment.sidSearch(club, uidVal, sidVal).setStatus("Cancelled");
		    	}
		    	System.out.println(Shipment.sidSearch(club, 0, sidVal));
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber changedelivery", options );
			    
		    
		}
		    
	}
	
	private void deliveryUpdate(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option dow = new Option("dow", "dow", true, "Day of the Week for Subscriber to recieve shipment" );
		dow.setRequired(true);
		options.addOption(dow);
		
		Option tod = new Option("tod", "tod", true, "Time of Day for Subscriber to recieve shipment" );
		tod.setRequired(true);
		options.addOption(tod);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    String dowVal = line.getOptionValue("dow");
		    String todVal = line.getOptionValue("tod");
		    try{
		    	Subscriber.setDeliveryTime(club, uidVal, dowVal, todVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber deliveryupdate", options );
			    
		    
		}
		    
	}
	
	private void getDelivery(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    try{
		    	Subscriber.getDeliveryTime(this.club, uidVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber delivery", options );
			    
		    
		}
		    
	}
	
	private void submitWineRating(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		Option rating = new Option("r", "rating", true, "Rating Of the Wine" );
		rating.setRequired(true);
		options.addOption(rating);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    int rVal = Integer.parseInt(line.getOptionValue("rating"));
		    try{
		    	Wine.addRatingSearch(club, uidVal, widVal, rVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber addrating", options );
			    
		    
		}
		    
	}
	
	private void getWineRating(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    try{
		    	System.out.println(Wine.ratingSearch(club, uidVal, widVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber rating", options );
			    
		    
		}
		    
	}
	
	private void deleteWineNote(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		Option nid = new Option("nid", "nid", true, "NID Of the Note" );
		nid.setRequired(true);
		options.addOption(nid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    int nidVal = Integer.parseInt(line.getOptionValue("nid"));
		    try{
		    	WineNote.deleteWineNote(club, uidVal, widVal, nidVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber deletewinenote", options );
			    
		    
		}
		    
	}
	
	private void updateWineNote(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		Option nid = new Option("nid", "nid", true, "NID Of the Note" );
		nid.setRequired(true);
		options.addOption(nid);
		
		Option content = new Option("c", "content", true, "Content Of the Note" );
		content.setRequired(true);
		options.addOption(content);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    int nidVal = Integer.parseInt(line.getOptionValue("nid"));
		    String contentVal = line.getOptionValue("content");
		    try{
		    	WineNote.updateWineNote(club, contentVal, uidVal, widVal, nidVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber updatewinenote", options );
			    
		    
		}
		    
	}
	
	private void searchWineNotes(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		Option nid = new Option("nid", "nid", true, "NID Of the Note" );
		nid.setRequired(true);
		options.addOption(nid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    int nidVal = Integer.parseInt(line.getOptionValue("nid"));
		    try{
		    	System.out.println(WineNote.nidSearch(club, uidVal, widVal, nidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber winenotesearch", options );
			    
		    
		}
		    
	}
	
	private void createWineNote(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    try{
		    	WineNote.createWineNote(this.club, uidVal, widVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber createwinenote", options );
			    
		    
		}
		    
	}
	
	private void getWineNotes(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    try{
		    	System.out.println(WineNote.widSearch(this.club, uidVal, widVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber winenotes", options );
			    
		    
		}
		    
	}
	
	private void searchWines(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option wid = new Option("wid", "wid", true, "WID Of the Wine" );
		wid.setRequired(true);
		options.addOption(wid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int widVal = Integer.parseInt(line.getOptionValue("wid"));
		    try{
		    	System.out.println(Wine.uidwidSearch(this.club, uidVal, widVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber winesearch", options );
			    
		    
		}
		    
	}
	
	private void getWines(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    try{
		    	System.out.println(Wine.uidSearch(this.club, uidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber wines", options );
			    
		    
		}
		    
	}
	
	private void deleteNote(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		Option nid = new Option("nid", "nid", true, "NID Of the Note" );
		nid.setRequired(true);
		options.addOption(nid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    int nidVal = Integer.parseInt(line.getOptionValue("nid"));
		    try{
		    	Note.deleteNote(club, uidVal, sidVal, nidVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber deletenote", options );
			    
		    
		}
		    
	}
	
	private void updateNote(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		Option nid = new Option("nid", "nid", true, "NID Of the Note" );
		nid.setRequired(true);
		options.addOption(nid);
		
		Option content = new Option("c", "content", true, "Content Of the Note" );
		content.setRequired(true);
		options.addOption(content);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    int nidVal = Integer.parseInt(line.getOptionValue("nid"));
		    String contentVal = line.getOptionValue("content");
		    try{
		    	Note.updateNote(club, uidVal, sidVal, nidVal, contentVal);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber updatenote", options );
			    
		    
		}
		    
	}
	
	private void searchNotes(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		Option nid = new Option("nid", "nid", true, "NID Of the Note" );
		nid.setRequired(true);
		options.addOption(nid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    int nidVal = Integer.parseInt(line.getOptionValue("nid"));
		    try{
		    	System.out.println(Note.nidSearch(club, uidVal, sidVal, nidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber notesearch", options );
			    
		    
		}
		    
	}
	
	private void createNote(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		Option content = new Option("c", "content", true, "Content Of the Note" );
		content.setRequired(true);
		options.addOption(content);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    String contentVal = line.getOptionValue("content");
		    try{
		    	Note n = new Note(contentVal, uidVal, sidVal);
		    	club.addNote(n);
		    	System.out.println(n);
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber createnote", options );
			    
		    
		}
		    
	}
	
	private void shipmentNotes(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
	
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    try{
		    	System.out.println(Note.sidSearch(club, uidVal, sidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber shipmentnotes", options );
			    
		    
		}
		    
	}
	
	
	
	private void shipmentSearchSubscriber(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option sid = new Option("sid", "sid", true, "SID Of the Shipment" );
		sid.setRequired(true);
		options.addOption(sid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    int sidVal = Integer.parseInt(line.getOptionValue("sid"));
		    try{
		    	System.out.println(Shipment.sidSearch(this.club, uidVal, sidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber shipmentsearch", options );
			    
		    
		}
		    
	}
	
	private void shipmentsSubscriber(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    try{
		    	System.out.println(Shipment.uidSearch(this.club, uidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber shipments", options );
			    
		    
		}
		    
	}
	
	private void searchSubscriber(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		options.addOption( "k", "query", true, "Query to search for" );
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    String qVal = "";
		    if(line.hasOption("query"))
		    {
		    qVal = line.getOptionValue("query");
		    }
		    try{
		    	System.out.println(QuerySearch.querySearch(this.club, uidVal, qVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber search ", options );
			    
		    
		}
		    
	}
	
	private void viewSubscriber(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    try{
		    	System.out.println(Subscriber.getSubscriber(this.club, uidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber view ", options );
			    
		    
		}
		    
	}
	
	private void modifySubscriber(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option uid = new Option("uid", "uid", true, "UID Of the Subscriber" );
		uid.setRequired(true);
		options.addOption(uid);
		
		Option name = new Option("n", "name", true, "Name Of the Subscriber" );
		options.addOption(name);
		
		Option email = new Option("e", "email", true, "Email Of the Subscriber" );
		options.addOption(email);
		
		Option address = new Option("a", "address", true, "Address Of the Subscriber" );
		options.addOption(address);
		
		Option city = new Option("c", "city", true, "City Of the Subscriber" );
		options.addOption(city);
		
		Option state = new Option("s", "state", true, "State Of the Subscriber" );
		options.addOption(state);
		
		Option zip = new Option("z", "zip", true, "Zip Code" );
		options.addOption(zip);
		
		Option phone = new Option("h", "phone", true, "Phone Number of the Subscriber" );
		options.addOption(phone);
		
		options.addOption( "f", "facebook", false, "Facebook" );
		options.addOption( "t", "twitter", false, "Twitter" );
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    
		    
		    
		    // Way to get each Value
		    
		    String fVal  ="";
		    String tVal  ="";
		    String stateVal = "";
		    String nameVal = "";
		    String emailVal = "";
		    String cityVal = "";
		    String zipVal = "";
		    String addVal = "";
		    String phoneVal = "";
		    int uidVal = Integer.parseInt(line.getOptionValue("uid"));
		    if(line.hasOption("state"))
		    {
		    stateVal = line.getOptionValue("state");
		    }
		    if(line.hasOption("name"))
		    {
		    nameVal = line.getOptionValue("name");
		    }
		    if(line.hasOption("email"))
		    {
		    emailVal = line.getOptionValue("email");
		    }
		    if(line.hasOption("city"))
		    {
		    cityVal = line.getOptionValue("city");
		    }
		    if(line.hasOption("zip"))
		    {
		    zipVal = line.getOptionValue("zip");
		    }
		    if(line.hasOption("address"))
		    {
		    addVal = line.getOptionValue("address");
		    }
		    if(line.hasOption("phone"))
		    {
		    phoneVal = line.getOptionValue("phone");
		    }
		    if(line.hasOption("facebook")){
		    	
		    	fVal  = line.getOptionValue("facebook");
		    }
		    
		    if(line.hasOption("twitter")){
		    	
		    	tVal  = line.getOptionValue("twitter");
		    }
		    
		    
		    // Modify Subscriber
		    
		    
		    try{
		    	Subscriber.getSubscriber(this.club, uidVal).updateInfo(nameVal, emailVal, phoneVal, fVal, tVal);
		    	System.out.println(Subscriber.getSubscriber(this.club, uidVal));
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber modify ", options );
			    
		    
		}
	}
	
	private void loadSubscriber(String args[]){
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option jfile = new Option("f", "jfile", true, "Path for JSON File containing Subscriber" );
		jfile.setRequired(true);
		options.addOption(jfile);
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    String fVal = line.getOptionValue("jfile");
		    JSONObject object = readJSONObject(fVal);
			if(object == null){
				System.out.println("Invalid Json File");
			}
			 	String stateVal = object.getJSONObject("address").getString("state");
			    String nameVal = object.getString("name");
			    String emailVal = object.getString("email");
			    String cityVal = object.getJSONObject("address").getString("city");
			    String zipVal = object.getJSONObject("address").getString("zip");
			    String addVal = object.getJSONObject("address").getString("street");
			    String phoneVal = object.getString("phone");
			    
			    String fbVal  ="";
			    String tVal  ="";
			    
			    if(object.has("facebook")){
			    	
			    	fbVal  = object.getString("facebook");
			    }
			    
			    if(object.has("twitter")){
			    	
			    	tVal  = object.getString("twitter");
			    }
			    
		    try{
		    	AddSubscriberRequest a = new AddSubscriber(addVal, cityVal, stateVal, zipVal, nameVal, emailVal, phoneVal, tVal, fbVal);
				AddSubscriberResponse r = a.addAccount(this.club.subs);
				r.printResponse();
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber load", options );
			    
		    
		}
		    
	}
	
	private void addSubscriber(String args[]) {
		
		CommandLineParser parser = new BasicParser();

		// create the Options
		Options options = new Options();
		
		Option name = new Option("n", "name", true, "Name Of the Subscriber" );
		name.setRequired(true);
		options.addOption(name);
		
		Option email = new Option("e", "email", true, "Email Of the Subscriber" );
		email.setRequired(true);
		options.addOption(email);
		
		Option address = new Option("a", "address", true, "Address Of the Subscriber" );
		address.setRequired(true);
		options.addOption(address);
		
		Option city = new Option("c", "city", true, "City Of the Subscriber" );
		city.setRequired(true);
		options.addOption(city);
		
		Option state = new Option("s", "state", true, "State Of the Subscriber" );
		state.setRequired(true);
		options.addOption(state);
		
		Option zip = new Option("z", "zip", true, "Zip Code" );
		zip.setRequired(true);
		options.addOption(zip);
		
		Option phone = new Option("h", "phone", true, "Phone" );
		phone.setRequired(true);
		options.addOption(phone);
		
		options.addOption( "f", "facebook", false, "Facebook" );
		options.addOption( "t", "twitter", false, "Twitter" );
		
		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );
		    
		    
		    
		    // Way to get each Value
		    String stateVal = line.getOptionValue("state");
		    String nameVal = line.getOptionValue("name");
		    String emailVal = line.getOptionValue("email");
		    String cityVal = line.getOptionValue("city");
		    String zipVal = line.getOptionValue("zip");
		    String addVal = line.getOptionValue("address");
		    String phoneVal = line.getOptionValue("phone");
		    
		    String fVal  ="";
		    String tVal  ="";
		    
		    if(line.hasOption("facebook")){
		    	
		    	fVal  = line.getOptionValue("facebook");
		    }
		    
		    if(line.hasOption("twitter")){
		    	
		    	tVal  = line.getOptionValue("twitter");
		    }
		    
		    
		    // Add Subscriber
		    
		    
		    try{
		    	
		    	
		    	AddSubscriberRequest a = new AddSubscriber(addVal, cityVal, stateVal, zipVal, nameVal, emailVal, phoneVal, tVal, fVal);
				AddSubscriberResponse r = a.addAccount(this.club.subs);
				r.printResponse();
		    	
		    }catch(Exception e){
		    	// Print Application Error Messages\\
		    	System.out.println(e);
		    }
		    
		    
		}
		catch( ParseException exp ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "java -classpath \".:lib/commons-cli-1.2.jar:lib/json-20141113.jar:lib/junit-4.12.jar:bin/\" edu.iit.cs445.vin.Main subscriber add ", options );
			    
		    
		}

		
		

}}