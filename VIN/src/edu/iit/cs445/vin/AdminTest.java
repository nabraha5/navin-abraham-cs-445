package edu.iit.cs445.vin;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Test;

public class AdminTest {
	//Club c = new Club("VIN");
	//Admin a = new Admin("Navin Abraham");
	//c.addAdmin(a);
	
	@Test
    public void testAdminSearch() {
		Club c = new Club("VIN");
		Admin a = new Admin("Navin Abraham");
		c.addAdmin(a);
		assertEquals(Admin.adminSearch(c, a.getAID()), a);
    }
	
	@Test
    public void fail_admin_search() {
		Club c = new Club("VIN");
		Admin a = new Admin("Navin Abraham");
		c.addAdmin(a);
		assertNotEquals(Admin.adminSearch(c, 1), a);
    }
	
	@Test
	 public void test_admin_update() {
			Club c = new Club("VIN");
			Admin a = new Admin("Navin Abraham");
			c.addAdmin(a);
			Admin.adminUpdate(c, a.getAID(), "Kevin Abraham");
			assertEquals(Admin.adminSearch(c, a.getAID()).getName(), "Kevin Abraham");
	    }
	
	@Test
	public void fail_revenue_start_end() {
		Club c = new Club("VIN");
		Shipment s1 = new Shipment();
		Shipment s2 = new Shipment();
		Shipment s3 = new Shipment();
		c.addShipment(s1);
		c.addShipment(s2);
		c.addShipment(s3);
		Admin.adminRevenueStartEnd(c, "2015-05", "2015-06");
    }
	
	@Test
	public void pass_revenue_start_end() {
		Club c = new Club("VIN");
		Shipment s1 = new Shipment();
		Shipment s2 = new Shipment();
		Shipment s3 = new Shipment();
		c.addShipment(s1);
		c.addShipment(s2);
		c.addShipment(s3);
		Admin.adminRevenueStartEnd(c, "2015-03", "2015-05");
    }
	
	
	@Test
	public void test_revenue_all() {
		Club c = new Club("VIN");
		Shipment s1 = new Shipment();
		Shipment s2 = new Shipment();
		Shipment s3 = new Shipment();
		c.addShipment(s1);
		c.addShipment(s2);
		c.addShipment(s3);
		Admin.adminRevenueAll(c);
		//assertEquals(Admin.adminRevenueAll(c), "Wine Revenue: 179.97\nDelivery Revenue: 18.03");
	}
	
	@Test
	public void testCreateGetMS() {
		Club c = new Club("VIN");
		Admin.createMS(c, "AR", "2015-04", null);
		assertEquals(Admin.getMonthlySelections(c), c.getMonthlySelections());
	}
	
	
	
	@Test
	public void pass_search_MS() {
		Club c = new Club("VIN");
		MonthlySelection ms = new MonthlySelection();
		c.addMS(ms);
		assertEquals(Admin.searchMonthlySelections(c, ms.getMID()), ms);
	}
	
	@Test
	public void fail_search_MS() {
		Club c = new Club("VIN");
		MonthlySelection ms = new MonthlySelection();
		c.addMS(ms);
		assertNotEquals(Admin.searchMonthlySelections(c, 1), ms);
	}
	
}
