package edu.iit.cs445.vin;

import java.time.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.io.Serializable;

import org.json.JSONObject;

public class Shipment implements Serializable {
	private YearMonth ym;
	private int SID;
	private int UID;
	private int numberOfCases;
	private String status;
	private double wineCost;
	private double shippingCost;
	private MonthlySelection ms;

	@Override
    public String toString() {
    	
    	JSONObject jsonObj   = new JSONObject(this);
    	
    	return jsonObj.toString();
    }
	
	public YearMonth getYm() {
		return ym;
	}

	public int getSID() {
		return SID;
	}

	public int getUID() {
		return UID;
	}

	public int getNumberOfCases() {
		return numberOfCases;
	}

	public String getStatus() {
		return status;
	}

	public double getWineCost() {
		return wineCost;
	}

	public double getShippingCost() {
		return shippingCost;
	}
	
	public MonthlySelection getMonthlySelection() 
	{
		return ms;
	}

	
	public void setStatus(String status) {
		this.status = status;
	}

	//Add code for canceling shipments if time
	public Shipment() 
	{
		this.ym = YearMonth.now();
		this.SID = IdGenerator.newID();
		this.status = "Pending";
		this.numberOfCases = 1;
		this.wineCost = 59.99*numberOfCases;
		this.shippingCost = 6.01*numberOfCases;
	}
	
	public Shipment(String ym, int UID, MonthlySelection ms)
	{
		this.ym = YearMonth.parse(ym);
		this.SID = IdGenerator.newID();
		this.UID = UID;
		this.numberOfCases = 1;
		this.status = "Pending";
		this.ms = ms;
		this.wineCost = 59.99*numberOfCases;
		this.shippingCost = 6.01*numberOfCases;
	}

	public void updateNumberOfCases(int numberofcases)
	{
		this.numberOfCases = numberofcases;
	}
	
public static Shipment sidSearch(Club c, int uid, int sid)
	{
		Iterator iter = c.getShipments().iterator();
		while (iter.hasNext()) 
		{
		    Object shipment = iter.next();
		    if (((Shipment)shipment).getUID() == uid &&
		    		((Shipment)shipment).getSID() == sid)
		    {
		    	return((Shipment)shipment);
		    }
		}
		System.out.println("UID/SID not found");
		return null;
	}
	
public static Collection<Shipment> uidSearch(Club c, int UID)
{
	Collection<Shipment> sset = new HashSet<Shipment>();
	Iterator iter = c.getShipments().iterator();
	while (iter.hasNext()) 
	{
	    Object shipment = iter.next();
	    if (((Shipment)shipment).UID == UID)
	    {
	    	sset.add((Shipment)shipment);
	    }
	}
	return sset;
}
	
public static Collection<Shipment> shipmentSearch(Club c, String query) 
{
	Collection<Shipment> sset = new HashSet<Shipment>();
	Iterator iter = c.getShipments().iterator();
	while (iter.hasNext()) 
	{
	    Object shipment = iter.next();
	    if (((Shipment)shipment).isMatch(query) ||
	    		query == "")
	    {
	    	sset.add((Shipment)shipment);
	    }
	}
	
	return sset;
}

public static Shipment shipmentCheck(Shipment shipment, String query) 
{
	    if (shipment.isMatch(query) ||
	    		query == "")
	    {
	    	return shipment;
	    }
	
	
	return null;
}

public boolean isMatch(String kw) {
    if (isMatchYM(kw)) {
            return true;
    } else return false;
}
    
private boolean isMatchYM(String kw) {
	String regex = "(?i).*" + kw + ".*";
    return this.ym.toString().matches(regex);
}


}
