package edu.iit.cs445.vin;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class DeliveryPartner {
	public static Collection<Subscriber> getDeliveries(Club c)
	{
		Collection<Subscriber> sset = new HashSet<Subscriber>();
		Iterator iter = c.getShipments().iterator();
		while (iter.hasNext()) 
		{ 
			Object shipment = iter.next();
			if (((Shipment)shipment).getStatus().equalsIgnoreCase("Pending"))
			{
				Iterator it = c.getSubs().iterator();
				while (it.hasNext()) 
				{ 
					Object sub = it.next();
					if (((Shipment)shipment).getUID() == ((Subscriber)sub).getID())
					{
						sset.add((Subscriber)sub);
					}
				}
			}
		}
		return sset;
	}
}
