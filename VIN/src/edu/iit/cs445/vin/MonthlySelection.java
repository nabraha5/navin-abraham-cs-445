package edu.iit.cs445.vin;

import java.time.*;
import java.util.*;
import java.io.Serializable;

import org.json.JSONObject;

public class MonthlySelection implements Serializable {
	private int MID;
	private String mst;
	private YearMonth ym;
	private Collection<Wine> ms = new HashSet<Wine>();
	
	public String getMst() {
		return mst;
	}

	public YearMonth getYm() {
		return ym;
	}
	
	@Override
    public String toString() {
    	
    	JSONObject jsonObj   = new JSONObject(this);
    	
    	return jsonObj.toString();
    }
	
	public boolean isMatch(String kw) {
		Iterator<Wine> it = this.ms.iterator();
		while (it.hasNext()) {
			Wine w = it.next();
			if (w.isMatch(kw)) return true;
		}
		return false;
	}
	
	public MonthlySelection() {
		this.MID = IdGenerator.newID();
		this.mst = "AR";
		this.ym = YearMonth.now();
		addWine(new Wine());
		addWine(new Wine());
		addWine(new Wine());
		addWine(new Wine());
		addWine(new Wine());
		addWine(new Wine());
	}
	
	public MonthlySelection(String mst, String ym, Collection<Wine> ms) {
		this.MID = IdGenerator.newID();
		this.mst = mst;
		this.ym = YearMonth.parse(ym);
		this.ms = ms;// next month's selection
	}

	public void addWine(Wine w)
	{
		this.ms.add(w);
	}

	public Collection<Wine> getWines()
	{
		return this.ms;
	}
	
	public int getMID()
	{
		return this.MID;
	}
}