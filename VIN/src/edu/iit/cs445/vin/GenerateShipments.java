package edu.iit.cs445.vin;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.*;

public class GenerateShipments implements Serializable {

	public static void generateShipments(Club c, String mst, String ym, MonthlySelection ms) {
    
		Iterator iter = c.getSubs().iterator();
		while (iter.hasNext()) 
		{ 
			Object sub = iter.next();
			if (((Subscriber)sub).getPreference().equalsIgnoreCase(mst))
			{
				Shipment sh = new Shipment(ym, ((Subscriber)sub).getID(), ms);
				c.addShipment(sh);
			}
		}
	}
}




