package edu.iit.cs445.vin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;

import org.json.JSONObject;

public class Main implements Serializable {
	
	public static void main(String[] args) throws IOException {	

		
		Club club = getClub();
			
			CommandInterpreter command = new CommandInterpreter(club);
			
			command.processArguments(Arrays.asList(args));
		
		save(club);
		
	}
	
	private static Club getClub() {
		Club club = load();
		
		if(club == null){
			
			club = new Club("VIN");
		}
		
		return club;
	}
	
	private static void save(Club club){
		
		try {
			
			File filePath = new File("navin-abraham.ser");
			
			
			OutputStream file = new FileOutputStream(filePath);
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);

			output.writeObject(club);
			output.flush();
			file.close();

		} catch (Exception e) {
			System.out.println("FIle Write Error");
		}

		
	}
	
	
	private static Club load() {
		
		try{
			File fileLocation = new File("navin-abraham.ser");
			
			if(!fileLocation.exists()) {
				return null;
			}
			
			InputStream file = new FileInputStream(fileLocation);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream(buffer);

			Club club = (Club) input.readObject();
			
			return club;
		
		}catch(Exception e){
			System.out.println("File Read Error");
		}
		
		return null;
	}
	
	
}