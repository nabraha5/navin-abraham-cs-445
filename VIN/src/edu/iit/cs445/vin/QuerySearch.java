package edu.iit.cs445.vin;

import java.io.Serializable;
import java.util.*;

public class QuerySearch implements Serializable {
	
	public static Collection<Object> querySearch(Club c, int uid, String query) 
	{
	Collection<Object> o = null;
	Collection<Shipment> uidshipments = Shipment.uidSearch(c, uid);
	Collection<Note> uidnotes = Note.uidSearch(c, uid);
	Iterator iter = uidshipments.iterator();
	while (iter.hasNext()) 
	{
	    Object shipment = iter.next();
	    MonthlySelection ms = ((Shipment)shipment).getMonthlySelection();
	o.add(Wine.wineSearch(ms.getWines(), query));
	o.add(Shipment.shipmentCheck(((Shipment)shipment), query));
	}
	Iterator i = uidnotes.iterator();
	while (i.hasNext()) 
	{
		Object note = i.next();
		o.add(Note.noteCheck(((Note)note), query));
	}
	return o;
	}
}
