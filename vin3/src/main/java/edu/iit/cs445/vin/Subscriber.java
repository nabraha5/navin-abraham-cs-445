package edu.iit.cs445.vin;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.io.Serializable;

import org.json.JSONObject;

public class Subscriber implements Serializable {
    private String name;
	private String email;
	private String phone;
	private String twitter;
	private String facebook;
    private Address address;
    private String dow;
    private String tod;
    private String mst;
    private int ID;
    
    
    
    
    public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public String getTwitter() {
		return twitter;
	}

	public String getFacebook() {
		return facebook;
	}

	public Address getAddress() {
		return address;
	}

	public String getDow() {
		return dow;
	}

	public String getTod() {
		return tod;
	}
	
	public static void main(String[] args) {
		
		Collection<Subscriber> sus = new HashSet<Subscriber>();
		
		sus.add(new Subscriber());
		sus.add(new Subscriber());
		
				
		System.out.println(sus);
		
		
	}

	@Override
    public String toString() {
    	
    	JSONObject jsonObj   = new JSONObject(this);
    	
    	return jsonObj.toString();
    }
    
    public Subscriber() {
    	this.name = "Jane Doe";
    	this.email = "jane.doe@example.com";
    	this.phone = "1234567890";
    	this.address = new Address();
    	this.mst = "RW";
    	this.ID = IdGenerator.newID();
    	this.dow = "Monday";
    	this.tod = "AM";
    }
    public Subscriber (String name, String email, String phone, Address address) {
    	this.name = name;
    	this.email = email;
    	this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
    	this.address = address;
    	this.mst = "RW";
    	this.ID = IdGenerator.newID();
    	this.tod = "Monday";
    	this.dow = "AM";
    }
    public Subscriber (String name, String email, String phone, Address address, String fb, String tw) {
    	this.name = name;
    	this.email = email;
    	this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
    	this.address = address;
    	this.twitter = tw;
    	this.facebook = fb;
    	this.mst = "RW";
    	this.ID = IdGenerator.newID();
    	this.tod = "Monday";
    	this.dow = "AM";
    }

    private boolean isMatchName(String kw) {
    	String regex = "(?i).*" + kw + ".*";
    	return this.name.matches(regex);
    }

    private boolean isMatchEmail(String kw) {
    	String regex = "(?i).*" + kw + ".*";
    	return this.email.matches(regex);
    }

    private boolean isMatchPhone(String kw) {
    	String s = kw.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters from search string
    	String regex = "(?i).*" + s + ".*";
    	return this.phone.matches(regex);
    }
    public boolean isMatch(String kw) {
    	if (isMatchName(kw) || isMatchEmail(kw) || isMatchPhone(kw)) {
    		return true;
    	} else return false;
    }

    public int getID() {
    	return this.ID;
    }

    public void updateInfo(String name, String email, String phone, String facebook, String twitter) {
    	this.name = name;
    	this.email = email;
    	this.phone = phone;
    	this.facebook = facebook;
    	this.twitter = twitter;
    }
    
    public String getPreference() {
    	return mst;
    }
    
    public void setPreference(String t) {
    	this.mst = t;
    }

    public static void setDeliveryTime(Club c, int uid, String dow, String tod)
    {
    	Iterator iter = c.subs.iterator();
		while (iter.hasNext()) 
		{
		    Object sub = iter.next();
		    if (uid == ((Subscriber)sub).ID)
		    {
		    	((Subscriber)sub).dow = dow;
		    	((Subscriber)sub).tod = tod;
		    }
		}
    }
    
    public static void getDeliveryTime(Club c, int uid)
    {
    	Iterator iter = c.subs.iterator();
		while (iter.hasNext()) 
		{
		    Object sub = iter.next();
		    if (uid == ((Subscriber)sub).ID)
		    {
		    	System.out.println("dow: " + ((Subscriber)sub).dow + "\ntod: " + ((Subscriber)sub).tod);
		    	//return {((Subscriber)sub).dow, ((Subscriber)sub).tod};
		    }
		}
    }
    
    public static Subscriber getSubscriber(Club c, int ID)
    {
    	
    		Iterator iter = c.getSubs().iterator();
    		while (iter.hasNext()) 
    		{
    		    Object sub = iter.next();
    		    if (ID == ((Subscriber)sub).ID)
    		    {
    		    	
    		    	return ((Subscriber)sub);
    		    }
    		}
			System.out.println("UID not found");
    		return null;
    }
	
    public String getEmail() {
		return this.email;
	}
	
}
