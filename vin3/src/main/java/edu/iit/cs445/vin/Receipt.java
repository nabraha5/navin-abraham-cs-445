package edu.iit.cs445.vin;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import org.json.JSONObject;

public class Receipt {
	private int RID;
	private int UID;
	private String name;
	private String recievedBy;
	private LocalDateTime createDate;

	public static void main(String[] args) {
		
		Collection<Receipt> admins = new HashSet<Receipt>();
		
		admins.add(new Receipt());
		admins.add(new Receipt());
		
				
		System.out.println("Receipts: " + admins);
		
		
	}

	@Override
    public String toString() {
    	
    	JSONObject jsonObj   = new JSONObject(this);
    	
    	return jsonObj.toString();
    }
	
	public int getRID() {
		return RID;
	}

	public int getUID() {
		return UID;
	}

	public String getName() {
		return name;
	}

	public String getRecievedBy() {
		return recievedBy;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	Receipt()
	{
		this.RID = IdGenerator.newID();
		this.UID = 0;
		this.name = "Navin Abraham";
		this.recievedBy = "Navin Abraham";
		this.createDate = LocalDateTime.now();
	}
	
	Receipt(int uid, String name, String recievedBy)
	{
		this.RID = IdGenerator.newID();
		this.UID = uid;
		this.name = name;
		this.recievedBy = recievedBy;
		this.createDate = LocalDateTime.now();
	}
	
	public static Collection<Receipt> searchReceipts(Club c, int rid)
	{
	Collection<Receipt> rset = new HashSet<Receipt>();
	Iterator iter = c.receipts.iterator();
	while (iter.hasNext()) 
	{
	    Object r = iter.next();
	    if (((Receipt)r).RID == rid)
	    {
	    	rset.add((Receipt)r);
	    }
	}
	return rset;
	}
}
