package edu.iit.cs445.vin;

import java.util.*;
import java.io.Serializable;

public class Club implements Serializable {
	static final long serialVersionUID = 42L;
	private String name;
	Collection<Admin> admins;
	Collection<Subscriber> subs;
	Collection<MonthlySelection> monthlySelections;
	Collection<Shipment> shipments;
	Collection<Note> notes;
	Collection<WineNote> wineNotes;
	Collection<Wine> wines;
	Collection<Receipt> receipts;
	
	Club(String name)
	{
		this.name = name;
		this.admins = new HashSet<Admin>();
		this.subs = new HashSet<Subscriber>();
		this.monthlySelections = new HashSet<MonthlySelection>();
		this.shipments = new HashSet<Shipment>();
		this.notes = new HashSet<Note>();
		this.wineNotes = new HashSet<WineNote>();
		this.wines = new HashSet<Wine>();
		this.receipts = new HashSet<Receipt>();
	}
	
	public Collection<Receipt> getReceipts()
	{
		return this.receipts;
	}
	
	public Collection<MonthlySelection> getMonthlySelections()
	{
		return this.monthlySelections;
	}
	
	public Collection<Admin> getAdmins()
	{
		return this.admins;
	}
	
	public Collection<Subscriber> getSubs()
	{
		return this.subs;
	}
	
	public Collection<Shipment> getShipments()
	{
		return this.shipments;
	}

	public Collection<Note> getNotes()
	{
		return this.notes;
	}
	
	public void addAdmin(Admin a)
	{
		this.admins.add(a);
	}
	
	public void addSubscriber(Subscriber s)
	{
		this.subs.add(s);
	}
	
	public void addMS(MonthlySelection ms)
	{
		this.monthlySelections.add(ms);
	}
	
	public void addShipment(Shipment s)
	{
		this.shipments.add(s);
	}
	
	public void addNote(Note n)
	{
		this.notes.add(n);
	}
	
	public void addWN(WineNote wn)
	{
		this.wineNotes.add(wn);
	}
	
	public void addWine(Wine w)
	{
		this.wines.add(w);
	}
	
	public void addReceipt(Receipt r)
	{
		this.receipts.add(r);
	}
}
