package edu.iit.cs445.vin;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class IdGenerator implements Serializable {
	private static AtomicInteger nextID = new AtomicInteger();

	public static int newID() {
		return nextID.getAndIncrement();
	}

}
