package edu.iit.cs445.vin;

import java.io.Serializable;

public enum WineVariety implements Serializable {
	RED, 
	WHITE, 
	ROSE;
}
