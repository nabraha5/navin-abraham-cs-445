package edu.iit.cs445.vin;

import java.io.Serializable;

public enum WineType implements Serializable {
	TABLE, 
	SWEET, 
	SPARKLING;
}
