package edu.iit.cs445.vin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;

public class Main implements Serializable {
	
	public static void saveClubState(Club c) throws IOException {
		FileOutputStream fout = null;
		ObjectOutputStream oos = null;
		try {
			fout = new FileOutputStream("C:/Users/Owner/Desktop/navin-abraham.ser", true);
			oos = new ObjectOutputStream(fout);
			oos.writeObject(c);
		} catch (IOException e) {
		        e.printStackTrace();
		} finally {
			if(oos != null) {
				oos.close();
			} 
		}
	}
	
	public static Club restoreClubState(Club c) throws IOException {
		ObjectInputStream ois = null;
		try {
			FileInputStream fis = new FileInputStream("C:/Users/Owner/Desktop/navin-abraham.ser");
			ois = new ObjectInputStream(fis);
			c = (Club)ois.readObject();
			return c;
		} catch (IOException e) {
			System.err.println("Nothing to restore.\n");
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException caught in restoreClubState()");
			e.printStackTrace();
		}
			finally {
			if (ois != null) {
				ois.close();
			} 
		}
		return c;
	}


	public static void main(String[] args) throws IOException {	

		
		Club club = getClub();
		
		try{
			
			CommandInterpreter command = new CommandInterpreter(club);
			
			command.processArguments(Arrays.asList(args));
			
		}catch(Exception e){
			
System.out.println("generic help message like <java vin <subscriber/wine/selection/admin/> <process> ");
			
		}
		
		save(club);
	
	
	}
	
	private static Club getClub() {
		Club club = load();
		
		if(club == null){
			
			club = new Club("VIN");
		}
		
		return club;
	}
	
	private static void save(Club club){
		
		try {
			
			File filePath = new File("data.ser");
			
			
			OutputStream file = new FileOutputStream(filePath);
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);

			output.writeObject(club);
			output.flush();
			file.close();

		} catch (Exception e) {
			System.out.println("FIle Write Error");
		}

		
	}
	
	
	private static Club load() {
		
		try{
			File fileLocation = new File("data.ser");
			
			if(!fileLocation.exists()) {
				return null;
			}
			
			InputStream file = new FileInputStream(fileLocation);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream(buffer);

			Club club = (Club) input.readObject();
			
			return club;
		
		}catch(Exception e){
			System.out.println("File Read Error");
		}
		
		return null;
	}
	
	
}