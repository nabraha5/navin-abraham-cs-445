package edu.iit.cs445.vin;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Test;

public class NoteTest {
	@Test
	public void test_delete_note() {
		Club c = new Club("VIN");
		Note n = new Note();
		c.addNote(n);
		assertEquals(Note.nidSearch(c, 0, 0, n.getNID()), n);
		Note.deleteNote(c, 0, 0, n.getNID());
		assertEquals(Note.nidSearch(c, 0, 0, n.getNID()), null);
	}
	
	@Test
	public void pass_nid_search() {
		Club c = new Club("VIN");
		Note n = new Note();
		c.addNote(n);
		assertEquals(Note.nidSearch(c, 0, 0, n.getNID()), n);
	}
	
	@Test
	public void fail_nid_search() {
		Club c = new Club("VIN");
		Note n = new Note();
		c.addNote(n);
		assertNotEquals(Note.nidSearch(c, 0, 1, n.getNID()), n);
	}
	
	@Test
	public void test_update_note() {
		Club c = new Club("VIN");
		Note n = new Note();
		c.addNote(n);
		Note.updateNote(c, 0, 0, n.getNID(), "Hello");
		assertEquals(Note.nidSearch(c, 0, 0, n.getNID()).getContent(), "Hello");
	}
	
	@Test
	public void test_sid_search() {
		Club c = new Club("VIN");
		Note n1 = new Note();
		Note n2 = new Note();
		Note n3 = new Note();
		c.addNote(n1);
		c.addNote(n2);
		c.addNote(n3);
		assertEquals(Note.sidSearch(c, 0, 0), c.getNotes());
	}
}
